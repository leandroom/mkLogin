<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
Author  : Leandro
Date    : 08/06/2015
Company : Viatec
-->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>HotSpot - Login</title>
        
        <link href="estilo.css" media="screen" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="geral">
            <div id="topo">
                <img width="250" height="250" src="imagens/wifi.png" alt="wifi-logo" title="Wifi" />
            </div>
            
            <form name="formCadastro" action="" method="post">
            <div id="form">
                <ul>
                    <li>
                        <input type="text" name="username" placeholder="Login:" value="" required="required" />
                    </li>
                    <li>
                        <input type="password" name="password" placeholder="Password:" value="" required="required" />
                    </li>
                    <li style="text-align: center">
                        <input type="submit" value="Conectar" />
                    </li>
                </ul>
            </div>
            </form>
            
            <p>Se voc� ainda n�o possui dados para acesso, basta se <a class="destaque" href="cadastro.php">cadastrar</a>.</p>
            
        </div>
        <div>
            <img src="imagens/logo.jpg" alt="giga-logo" title="Gigacom" />
            <br />
            Desenvolvido por: <a href="mailto:leandroomagalhaes@gmail.com">Leandro Magalh�es</a>
        </div>
    </body>
</html>
s